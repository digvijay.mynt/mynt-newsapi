# Code setup in local Machine

## Prerequisite
```
node version v12.18.4 or Above
```

## Project setup (init)
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
