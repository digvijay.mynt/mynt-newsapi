import axios from "axios";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    isLoading: true,
    resultCount: 0,
    pageSize: 18,
    searchQuery: "apple",
    searchCountry: "",
    searchCategory: "",
    fetchData: [],
    BookMarksData: [],
  },
  getters: {
    getResultCount: (state) => {
      return state.resultCount;
    },
    getFetchNews: (state) => {
      return state.fetchData;
    },
    pageCount: (state) => {
      return state.pageSize;
    },
    BookMarksData: (state) => {
      return state.BookMarksData;
    },
    BookmarkCount: (state) => {
      return state.BookMarksData.length;
    },
    isLoading: (state) => {
      return state.isLoading;
    },
    dropDownData: (state) => {
      return {
        country: state.searchCountry,
        category: state.searchCategory,
        search: state.searchQuery,
      };
    },
  },
  mutations: {
    getData: async (state, payload = "") => {
      if (payload) {
        state.searchQuery = payload.query;
        state.searchCountry = payload.country;
        state.searchCategory = payload.category;
      }
      if (
        (state.searchQuery != "" && state.searchCategory != "") ||
        (state.searchQuery != "" && state.searchCountry != "")
      ) {
        if (state.searchCategory == "" || state.searchCountry == "") {
          return false;
        }
      }

      const url = `https://newsapi.org/v2/${
        state.searchCountry && state.searchCategory
          ? "top-headlines"
          : "everything"
      }?${state.searchCountry ? `country=${state.searchCountry}&` : ""}${
        state.searchCategory ? `category=${state.searchCategory}&` : ""
      }${state.searchQuery ? `q=${state.searchQuery}&` : ""}pageSize=${
        state.pageSize
      }&apiKey=0f7285bd3ca7434db434cb0fe0b5b489`;
      let { data, status } = await axios.get(url);
      if (status == 200) {
        const { totalResults, articles } = data;
        state.resultCount = totalResults;
        state.fetchData = articles.filter(
          (value, index) => (value.u_id = index + 1)
        );
        state.isLoading = false;
      }
      const { totalResults, articles } = data;
      state.resultCount = totalResults;
      state.fetchData = articles;
    },
    updatePageSize: (state, payload) => {
      state.pageSize = payload;
    },
    addBookmarks: (state, payload) => {
      state.BookMarksData.push(payload);
    },
    removeBookmarks: (state, payload) => {
      const filterBookmarkResult = state.BookMarksData.filter(
        (value) => JSON.stringify(value) != JSON.stringify(payload)
      );
      state.BookMarksData = filterBookmarkResult;
    },
  },
  actions: {
    getNewsData: ({ commit }, payload) => {
      commit("getData", payload);
    },

    DefaultNewsFetch: ({ commit }) => {
      commit("getData");
    },

    PageSizeUpdate: ({ commit }, payload) => {
      commit("updatePageSize", payload);
    },

    AddBookmarks: ({ commit }, payload) => {
      commit("addBookmarks", payload);
    },

    RemoveBookmarks: ({ commit }, payload) => {
      commit("removeBookmarks", payload);
    },
  },
});
